# HarperNode

HarperNode is a lite version of Hashnode build using Express/Node.js along with HarperDB.


> This repository contains the client side code

## Live Demo

Click [here](https://harpernode-client.vercel.app/) to view the complete app 

---

## Run locally

To run the code locally, create an `.env` file in the root directory to configure environment variables. The file should contain these props:


```
REACT_APP_GOOGLE_CLIENT_ID=<Google client ID for OAUTH>
REACT_APP_API_ENDPOINT=<Backend URL eg:(http://localhost:5000/)>
```


Then run the following commands:

1. To install dependencies

```
yarn install
```

2. To run the code

```
yarn start
```


