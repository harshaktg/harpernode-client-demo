import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { AppContextProvider } from "contexts/AppContext";
import { ThemeSwitcherProvider } from "react-css-theme-switcher";
import reportWebVitals from "./reportWebVitals";

const themes = {
  dark: `${process.env.PUBLIC_URL}/css/dark-theme.css`,
  light: `${process.env.PUBLIC_URL}/css/light-theme.css`,
};

ReactDOM.render(
  <React.StrictMode>
    <ThemeSwitcherProvider
      themeMap={themes}
      defaultTheme={
        localStorage.getItem("harperNodeTheme") === "dark" ? "dark" : "light"
      }
      insertionPoint="styles-insertion-point"
    >
      <AppContextProvider>
        <App />
      </AppContextProvider>
    </ThemeSwitcherProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
