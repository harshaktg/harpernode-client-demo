import { useContext } from "react";
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { AnimatePresence } from "framer-motion";
import AppContext from "contexts/AppContext";
import { ApplicationLayout, CommonLayout } from "components/layouts";
import Animation from "components/common/Animation";
import Loader from "components/common/Loader";
import NotFound from "components/common/NotFound";
import Profile from "pages/Profile";
import {
  ArticlesPage,
  BookmarksPage,
  FeedsPage,
  ProfilePage,
  RegisterPage,
  TagsPage,
  ViewArticlePage,
  ExplorePage,
  MyPostsPage,
  SearchPage,
  DevelopersPage,
} from "pages/index";
import WithAuthorized from "components/hoc/withAuthorized";

const AppWrapper = styled.div`
  .app-body {
    height: calc(100vh - 70px);
  }
  .flex {
    display: flex;
  }
  .al-ctr {
    align-items: center;
  }
  .jc-ctr {
    justify-content: center;
  }
  .j-end {
    justify-content: flex-end;
  }
  .f11 {
    flex: 1 1;
  }
  .theme-switcher {
    height: inherit;
    cursor: pointer;
  }
  .prof-btn {
    height: initial;
  }
  .prof-name {
    font-size: 16px;
  }
  .sp-btwn {
    justify-content: space-between;
  }
  .ant-menu-item {
    display: flex;
    align-items: center;
  }
  .ant-menu-inline,
  .ant-menu-vertical,
  .ant-menu-vertical-left {
    border-right: none;
  }
  .mt-20 {
    margin-top: 20px;
  }
  .mt-10 {
    margin-top: 10px;
  }
  .mb-10 {
    margin-bottom: 10px;
  }
  .mr-10 {
    margin-right: 10px;
  }
  .mr-20 {
    margin-right: 20px;
  }
  .mlr-10 {
    margin: 0 10px;
  }
  .mlr-20 {
    margin: 0 20px;
  }
  .mtb-10 {
    margin: 10px 0;
  }
  .mtb-20 {
    margin: 20px 0;
  }
  .fwrap {
    flex-wrap: wrap;
  }
  .rc-md-editor {
    background: inherit;
    .rc-md-navigation {
      background: inherit;
    }
    .editor-container {
      .sec-md {
        .input {
          background: inherit;
          color: inherit;
        }
      }
    }
  }
`;

const App = () => {
  const context = useContext(AppContext);
  return (
    <>
      <AppWrapper>
        <Router>
          <AnimatePresence exitBeforeEnter>
            {context.loading ? (
              <Animation>
                <Loader />
              </Animation>
            ) : (
              <Switch>
                <Route
                  path="/myfeed"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        {/* <WithAuthorized> */}
                        <FeedsPage />
                        {/* </WithAuthorized> */}
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/myposts"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <WithAuthorized>
                          <MyPostsPage />
                        </WithAuthorized>
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/explore"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <ExplorePage />
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/tags"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <TagsPage />
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/bookmarks"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <WithAuthorized>
                          <BookmarksPage />
                        </WithAuthorized>
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/search"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <SearchPage />
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/register"
                  exact
                  component={() => (
                    <CommonLayout>
                      <Animation>
                        <RegisterPage />
                      </Animation>
                    </CommonLayout>
                  )}
                />
                <Route
                  path="/article/view/:id"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <ViewArticlePage />
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/article/:type/:id"
                  exact
                  component={() => (
                    <CommonLayout>
                      <Animation>
                        <WithAuthorized>
                          <ArticlesPage />
                        </WithAuthorized>
                      </Animation>
                    </CommonLayout>
                  )}
                />
                <Route
                  path="/profile/view/:id"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <Profile />
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/profile/:type"
                  exact
                  component={() => (
                    <CommonLayout>
                      <Animation>
                        <WithAuthorized>
                          <ProfilePage />
                        </WithAuthorized>
                      </Animation>
                    </CommonLayout>
                  )}
                />
                <Route
                  path="/developers"
                  exact
                  component={() => (
                    <ApplicationLayout>
                      <Animation>
                        <DevelopersPage />
                      </Animation>
                    </ApplicationLayout>
                  )}
                />
                <Route
                  path="/:somepath"
                  component={() => (
                    <CommonLayout>
                      <Animation>
                        <NotFound />
                      </Animation>
                    </CommonLayout>
                  )}
                />
                <Redirect
                  to={{
                    pathname: "/myfeed",
                    from: "/",
                  }}
                />
              </Switch>
            )}
          </AnimatePresence>
        </Router>
      </AppWrapper>
    </>
  );
};

export default App;
