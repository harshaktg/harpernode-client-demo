import { useState } from "react";
import { Input, Button, Tooltip } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import styled from "styled-components";
import MarkdownEditor from "../components/inputs/MarkdownEditor";
import ArticleHeader from "../components/layouts/ArticleHeader";

const ArticleSheetWrapper = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 20px 0;
  textarea {
    border: none !important;
    box-shadow: none !important;
  }
  .title {
    font-size: 30px;
    font-weight: bold;
  }
  .subtitle {
    font-size: 24px;
    font-weight: 500;
  }
`;

const ArticleSheet = (props) => {
  const {
    content = "",
    isHidden = false,
    title = "",
    subTitle = "",
    enableSubtitle = false,
    tags = [],
    commentsDisabled = false,
    status = "DRAFT",
    submitLoading,
    action,
  } = props;
  const [currentTitle, setCurrentTitle] = useState(title || "");
  const [currentSubTitle, setCurrentSubTitle] = useState(subTitle || "");
  const [currentContent, setCurrentContent] = useState(content || "");
  const [subtitleEnabled, setSubtitleEnabled] = useState(
    enableSubtitle || false
  );
  const [currentCommentsDisabled, setCurrentCommentsDisabled] = useState(
    commentsDisabled || false
  );
  const [currentTags, setCurrentTags] = useState(tags || []);

  const submitHandler = () => {
    const reqOptions = {
      title: currentTitle,
      subTitle: currentSubTitle,
      content: currentContent,
      enableSubtitle: subtitleEnabled,
      commentsDisabled: currentCommentsDisabled,
      tags: currentTags,
    };
    action(reqOptions);
  };

  return (
    <ArticleSheetWrapper>
      <ArticleHeader
        data={{
          status,
          currentCommentsDisabled,
          setCurrentCommentsDisabled,
          currentTags,
          setCurrentTags,
          submitLoading,
          submitHandler,
        }}
      />
      <div className="mtb-20">
        <Input.TextArea
          placeholder="Enter title..."
          className="title"
          value={currentTitle}
          autoSize
          onChange={(e) => setCurrentTitle(e.target.value)}
        />
      </div>
      {subtitleEnabled && (
        <div className="flex mtb-20">
          <Input.TextArea
            placeholder="Enter subtitle..."
            className="subtitle"
            value={currentSubTitle}
            autoSize
            onChange={(e) => setCurrentSubTitle(e.target.value)}
          />
          <Tooltip title="Remove subtitle">
            <Button
              type="primary"
              shape="circle"
              danger
              className="mlr-10"
              icon={<CloseOutlined />}
              onClick={() => setSubtitleEnabled(!subtitleEnabled)}
            />
          </Tooltip>
        </div>
      )}
      {!subtitleEnabled && (
        <div className="mtb-20">
          <Button
            type="dashed"
            onClick={() => setSubtitleEnabled(!subtitleEnabled)}
          >
            Add subtitle
          </Button>
        </div>
      )}
      <MarkdownEditor
        // value={content || ''}
        value={currentContent || ""}
        updateHandler={setCurrentContent}
        placeholder="Your story goes here..."
        isHidden={isHidden || false}
      />
    </ArticleSheetWrapper>
  );
};

export default ArticleSheet;
