const customFetch = (url, opts) => {
  const defaultOpts = {
    credentials: "include",
    headers: {
      token: localStorage.getItem("harperNodeId"),
      "Content-Type": "application/json",
    },
  };
  return fetch(`${process.env.REACT_APP_API_ENDPOINT}${url}`, {
    ...defaultOpts,
    ...opts,
  });
};

export default customFetch;
