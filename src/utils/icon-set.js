import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMoon,
  faSun,
  // faNewspaper,
  // faCompass,
  faTags,
  // faBookmark,
  faSearch,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faCog,
  faUserEdit,
  faSignOutAlt,
  faUserPlus,
  faUserCheck,
  faUserTimes,
  faEllipsisV,
  faTheaterMasks,
  faInfoCircle,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import {
  faBookmark,
  faNewspaper,
  faCompass,
  faFileAlt,
} from "@fortawesome/free-regular-svg-icons";
import {
  faLinkedin,
  faMedium,
  faGitlab,
  faGithub,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";

library.add(
  faMoon,
  faSun,
  faNewspaper,
  faCompass,
  faTags,
  faBookmark,
  faSearch,
  faAngleDoubleRight,
  faAngleDoubleLeft,
  faCog,
  faUserEdit,
  faSignOutAlt,
  faUserPlus,
  faUserCheck,
  faUserTimes,
  faFileAlt,
  faEllipsisV,
  faTheaterMasks,
  faInfoCircle,
  faLinkedin,
  faMedium,
  faGitlab,
  faGithub,
  faTwitter,
  faUser
);
