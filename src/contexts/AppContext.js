import React, { useContext, useState, useEffect } from "react";
import customFetch from "utils/fetch";
import jwt_decode from "jwt-decode";

const AppContext = React.createContext("main");

export const AppContextProvider = (props) => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    if (localStorage.getItem("harperNodeId")) {
      getCurrentUser();
    }
  }, []);

  const getCurrentUser = async () => {
    setLoading(true);
    const res = await customFetch("currentUser", { method: "GET" });
    const data = await res.json();
    setLoading(false);
    if (data.error) {
      setError(data.error);
      setUser(null);
    } else {
      setUser(data);
      setError(null);
    }
  };

  const getUpdatedCurrentUser = async () => {
    const res = await customFetch("currentUser", { method: "GET" });
    const data = await res.json();
    if (data.error) {
      setError(data.error);
      setUser(null);
    } else {
      setUser(data);
      setError(null);
    }
  };

  const googleLogIn = async (googleData) => {
    setLoading(true);
    const res = await customFetch("auth/google", {
      method: "POST",
      body: JSON.stringify({
        token: googleData.tokenId,
      }),
    });
    const data = await res.json();
    setLoading(false);
    localStorage.setItem("harperNodeId", data.token);
    setUser(jwt_decode(data.token));
    if (data.error) {
      setError(data.error);
      throw new Error(data.error);
    }
  };

  const logOut = async () => {
    setLoading(true);
    const res = await customFetch("auth/logout", {
      method: "DELETE",
    });
    const data = await res.json();
    setLoading(false);
    if (data.success) {
      localStorage.removeItem("harperNodeId");
      setUser(null);
      setError(null);
    }
    if (data.error) {
      setError(data.error);
    }
  };

  return (
    <AppContext.Provider
      value={{
        user,
        loading,
        error,
        getCurrentUser,
        getUpdatedCurrentUser,
        googleLogIn,
        logOut,
      }}
      {...props}
    />
  );
};

export const useAuth = () => useContext(AppContext);
export default AppContext;
