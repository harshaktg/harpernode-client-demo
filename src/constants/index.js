const MENU_ITEMS = [
  {
    icon: ["far", "newspaper"],
    key: "myfeed",
    name: "My Feed",
  },
  {
    icon: ["far", "file-alt"],
    key: "myposts",
    name: "My Posts",
    secure: true,
  },
  {
    icon: ["far", "compass"],
    key: "explore",
    name: "Explore",
  },
  {
    icon: ["fas", "tags"],
    key: "tags",
    name: "Tags",
  },
  {
    icon: ["far", "bookmark"],
    key: "bookmarks",
    name: "Bookmarks",
    secure: true,
  },
  {
    icon: ["fas", "search"],
    key: "search",
    name: "Search",
  },
];

const OTHER_LINKS = [
  {
    icon: ["fas", "theater-masks"],
    key: "developers",
    name: "Developers",
  },
  // {
  //   icon: ["fas", "info-circle"],
  //   key: "aboutus",
  //   name: "About us",
  // },
];

const SOCIAL_LOGINS = [
  {
    name: "Google",
    logo: "google.png",
  },
  {
    name: "LinkedIn",
    logo: "linkedin.png",
  },
  {
    name: "GitHub",
    logo: "github.png",
  },
  {
    name: "Facebook",
    logo: "facebook.png",
  },
];

export { MENU_ITEMS, SOCIAL_LOGINS, OTHER_LINKS };
