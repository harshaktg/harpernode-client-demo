import { useState, useContext } from "react";
import { Card, Typography, Avatar, Tag, Button, message } from "antd";
import {
  HeartOutlined,
  HeartFilled,
  BookOutlined,
  BookFilled,
} from "@ant-design/icons";
import dayjs from "dayjs";
import customFetch from "utils/fetch";
import { withRouter } from "react-router-dom";
import { UserOutlined } from "@ant-design/icons";
import { HeartLike } from "assets/svg";
import styled from "styled-components";
import Animation from "components/common/Animation";
import AppContext from "contexts/AppContext";

const ArticleDetailsWrapper = styled.div`
  .like-count {
    font-weight: bold;
  }
  .user-detail {
    cursor: pointer;
  }
`;

const ArticleDetails = (props) => {
  const { data, history } = props;

  const context = useContext(AppContext);

  const [likes, setLikes] = useState(data.likes.length || 0);
  const [isLiked, setIsLiked] = useState(
    data.likes.includes(context?.user?.user_id) || false
  );
  const [isBookmarked, setIsBookmarked] = useState(
    context?.user?.bookmarks?.includes(data.post_id) || false
  );

  const [likeLoader, setLikeLoader] = useState(false);
  const [bookmarkLoader, setBookmarkLoader] = useState(false);

  const likeHandler = async () => {
    setLikeLoader(true);
    const res = await customFetch(`posts/like/${data.post_id}`, {
      method: "POST",
    });
    const { data: newData, error } = await res.json();
    setLikeLoader(false);
    if (newData) {
      message.success(newData);
      setIsLiked(true);
      setLikes(likes + 1);
    } else {
      message.error(error);
    }
  };

  const unlikeHandler = async () => {
    setLikeLoader(true);
    const res = await customFetch(`posts/unlike/${data.post_id}`, {
      method: "POST",
    });
    const { data: newData, error } = await res.json();
    setLikeLoader(false);
    if (newData) {
      message.success(newData);
      setIsLiked(false);
      setLikes(likes - 1);
    } else {
      message.error(error);
    }
  };

  const addBookmarkHandler = async () => {
    setBookmarkLoader(true);
    const res = await customFetch(`bookmarks/add`, {
      method: "POST",
      body: JSON.stringify({
        postId: data.post_id,
      }),
    });
    const { data: newData, error } = await res.json();
    setBookmarkLoader(false);
    if (newData) {
      context.getUpdatedCurrentUser();
      setIsBookmarked(true);
      message.success(newData);
    } else {
      message.error(error);
    }
  };

  const removeBookmarkHandler = async () => {
    setBookmarkLoader(true);
    const res = await customFetch(`bookmarks/remove`, {
      method: "POST",
      body: JSON.stringify({
        postId: data.post_id,
      }),
    });
    const { data: newData, error } = await res.json();
    setBookmarkLoader(false);
    if (newData) {
      context.getUpdatedCurrentUser();
      setIsBookmarked(false);
      message.success(newData);
    } else {
      message.error(error);
    }
  };

  return (
    <ArticleDetailsWrapper>
      <Animation>
        <Card>
          <Typography.Title level={4}>Author:</Typography.Title>
          <div
            className="flex mtb-10 al-ctr user-detail"
            onClick={() =>
              history.push(`/profile/view/${data.created_user?.user_id}`)
            }
          >
            <Avatar
              icon={<UserOutlined />}
              src={data.created_user?.picture}
              size="default"
              className="mr-10"
            />
            <Typography.Title level={5}>
              {data.created_user?.name}
            </Typography.Title>
          </div>
          <Typography.Title level={4}>Tags:</Typography.Title>
          <div className="mtb-10">
            {data.tags.map((tag) => (
              <Tag className="mb-10">{tag}</Tag>
            ))}
          </div>
          <Typography.Title level={4}>Published time:</Typography.Title>
          <Typography.Paragraph type="secondary">
            {dayjs(data.__createdtime__).format("MMM DD YYYY, hh:mm A")}
          </Typography.Paragraph>
          <Typography.Title level={4}>Likes:</Typography.Title>
          <div className="flex al-ctr mb-10">
            <HeartLike width="20" height="20" className="mr-10" />
            <div className="like-count">{likes}</div>
          </div>
          {context?.user && (
            <>
              <div className="mtb-10">
                {data.likes.includes(context?.user?.user_id) || isLiked ? (
                  <Button
                    type="primary"
                    onClick={unlikeHandler}
                    loading={likeLoader}
                    icon={<HeartFilled />}
                  >
                    Liked
                  </Button>
                ) : (
                  <Button
                    onClick={likeHandler}
                    loading={likeLoader}
                    icon={<HeartOutlined />}
                  >
                    Like
                  </Button>
                )}
              </div>
              <div>
                {context?.user?.bookmarks?.includes(data.post_id) ||
                isBookmarked ? (
                  <Button
                    type="primary"
                    onClick={removeBookmarkHandler}
                    loading={bookmarkLoader}
                    icon={<BookFilled />}
                  >
                    Bookmarked
                  </Button>
                ) : (
                  <Button
                    onClick={addBookmarkHandler}
                    loading={bookmarkLoader}
                    icon={<BookOutlined />}
                  >
                    Bookmark
                  </Button>
                )}
              </div>
            </>
          )}
        </Card>
      </Animation>
    </ArticleDetailsWrapper>
  );
};

export default withRouter(ArticleDetails);
