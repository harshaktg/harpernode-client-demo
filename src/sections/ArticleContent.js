import { Typography, Divider } from "antd";
import Markdown from "markdown-to-jsx";
import styled from "styled-components";

const ArticleContentWrapper = styled.div`
  height: calc(100vh - 110px);
  overflow: auto;
`;

const ArticleContent = (props) => {
  const { data } = props;
  return (
    <ArticleContentWrapper>
      <Typography.Title level={1}>{data.title}</Typography.Title>
      {data.enableSubTitle && (
        <Typography.Title level={4}>{data.subTitle}</Typography.Title>
      )}
      <Divider />
      <div>
        <Markdown>{data.content}</Markdown>
      </div>
    </ArticleContentWrapper>
  );
};

export default ArticleContent;
