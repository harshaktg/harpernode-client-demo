import AppContext from "contexts/AppContext";
import { useState, useEffect, useContext } from "react";
import { Card, Avatar, Typography } from "antd";
import { UserOutlined } from "@ant-design/icons";
import customFetch from "utils/fetch";
import { Empty } from "assets/svg";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import Animation from "components/common/Animation";
import UsersSkeleton from "components/skeletons/UsersSkeleton";

const EmptyWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const FollowingWrapper = styled.div`
  .profile-card-grid {
    width: calc(25% - 20px);
    margin: 10px;
    text-align: center;
    cursor: pointer;
    height: 180px;
  }
`;

const ProfileFollowing = (props) => {
  const context = useContext(AppContext);
  const { userId = context.user.user_id } = props;

  const [loading, setLoading] = useState(false);
  const [followingUsers, setFollowingUsers] = useState([]);

  const getfollowingUsers = async () => {
    setLoading(true);
    const res = await customFetch(`user/following/${userId}`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setFollowingUsers(data);
  };

  useEffect(() => {
    getfollowingUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return (
      <Animation>
        <UsersSkeleton />
      </Animation>
    );
  }

  return (
    <FollowingWrapper>
      <Animation>
        {followingUsers?.map((follower) => (
          <Card.Grid
            className="profile-card-grid"
            onClick={() =>
              follower.user_id === context?.user?.user_id
                ? props.history.push(`/profile/view`)
                : props.history.push(`/profile/view/${follower.user_id}`)
            }
          >
            <div>
              <Avatar
                size={80}
                icon={<UserOutlined />}
                src={follower.picture}
              />
              <Typography.Title level={5} strong className="mtb-10">
                {follower.name}
              </Typography.Title>
              <Typography.Text type="secondary">
                {follower.tagLine}
              </Typography.Text>
            </div>
          </Card.Grid>
        ))}
        {followingUsers?.length === 0 && (
          <EmptyWrapper>
            <Empty width="400" height="400" />
          </EmptyWrapper>
        )}
      </Animation>
    </FollowingWrapper>
  );
};

export default withRouter(ProfileFollowing);
