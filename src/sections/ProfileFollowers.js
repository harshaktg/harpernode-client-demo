import AppContext from "contexts/AppContext";
import { useState, useEffect, useContext } from "react";
import { Card, Avatar, Typography } from "antd";
import { isMobile } from "react-device-detect";
import { UserOutlined } from "@ant-design/icons";
import customFetch from "utils/fetch";
import { Empty } from "assets/svg";
import Animation from "components/common/Animation";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import UsersSkeleton from "components/skeletons/UsersSkeleton";

const EmptyWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const FollowersWrapper = styled.div`
  .profile-card-grid {
    width: ${(props) =>
      props.isMobile ? "calc(100% - 20px)" : `calc(25% - 20px)`};
    margin: 10px;
    text-align: center;
    cursor: pointer;
    height: ${(props) => (props.isMobile ? "initial" : `180px`)};
  }
`;

const ProfileFollowers = (props) => {
  const context = useContext(AppContext);
  const { userId = context.user.user_id } = props;

  const [loading, setLoading] = useState(false);
  const [followers, setFollowers] = useState([]);

  const getFollowers = async () => {
    setLoading(true);
    const res = await customFetch(`user/followers/${userId}`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setFollowers(data);
  };

  useEffect(() => {
    getFollowers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return (
      <Animation>
        <UsersSkeleton />
      </Animation>
    );
  }

  return (
    <FollowersWrapper isMobile={isMobile}>
      <Animation>
        {followers?.map((follower) => (
          <Card.Grid
            className="profile-card-grid"
            onClick={() =>
              follower.user_id === context?.user?.user_id
                ? props.history.push(`/profile/view`)
                : props.history.push(`/profile/view/${follower.user_id}`)
            }
          >
            <div>
              <Avatar
                size={80}
                icon={<UserOutlined />}
                src={follower.picture}
              />
              <Typography.Title level={5} strong className="mtb-10">
                {follower.name}
              </Typography.Title>
              <Typography.Text type="secondary">
                {follower.tagLine}
              </Typography.Text>
            </div>
          </Card.Grid>
        ))}
        {followers?.length === 0 && (
          <EmptyWrapper>
            <Empty width="400" height="400" />
          </EmptyWrapper>
        )}
      </Animation>
    </FollowersWrapper>
  );
};

export default withRouter(ProfileFollowers);
