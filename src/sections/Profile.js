import { useEffect, useState } from "react";
import {
  Divider,
  Row,
  Col,
  Typography,
  Tag,
  Button,
  Input,
  Select,
} from "antd";
import customFetch from "utils/fetch";
import {
  TwitterOutlined,
  GithubOutlined,
  FacebookFilled,
  LinkedinFilled,
  GlobalOutlined,
} from "@ant-design/icons";
import styled from "styled-components";

const ProfileWrapper = styled.div`
  .ant-divider-horizontal.ant-divider-with-text-left::before {
    width: 1%;
  }
  .skill-tag {
    padding: 0;
  }
  .w350 {
    width: 350px;
  }
`;

const Profile = (props) => {
  const {
    currentAvailableFor,
    setCurrentAvailableFor,
    availableFor,
    currentMoreAbout,
    setCurrentMoreAbout,
    moreAbout,
    currentSkills,
    setCurrentSkills,
    tags,
    currentTwitter,
    setCurrentTwitter,
    twitter,
    currentGithub,
    setCurrentGithub,
    github,
    currentFb,
    setCurrentFb,
    facebook,
    currentLinkedin,
    setCurrentLinkedin,
    linkedin,
    currentWebsite,
    setCurrentWebsite,
    website,
    isEdit,
  } = props;

  const [loading, setLoading] = useState(false);

  const [availableTags, setAvailableTags] = useState([]);

  const getAllTags = async () => {
    setLoading(true);
    const res = await customFetch(`tags`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setAvailableTags(data.map((tg) => tg.name));
  };

  useEffect(() => {
    getAllTags();
  }, []);

  return (
    <ProfileWrapper>
      <Row gutter={50}>
        <Col flex="1 1 300px">
          <Divider orientation="left">About</Divider>
          <Row className="mtb-20">
            <Col flex="1 1 180px">
              <Typography.Text strong>Available for</Typography.Text>
            </Col>
            <Col flex="1 1 180px">
              {isEdit ? (
                <Input.TextArea
                  placeholder="Enter Available for"
                  value={currentAvailableFor}
                  autoSize
                  onChange={(e) => setCurrentAvailableFor(e.target.value)}
                />
              ) : (
                <Typography.Text>{availableFor || "NA"}</Typography.Text>
              )}
            </Col>
          </Row>
          <Row className="mtb-20">
            <Col flex="1 1 180px">
              <Typography.Text strong>More about you</Typography.Text>
            </Col>
            <Col flex="1 1 180px">
              {isEdit ? (
                <Input.TextArea
                  placeholder="Enter something about you"
                  value={currentMoreAbout}
                  autoSize
                  onChange={(e) => setCurrentMoreAbout(e.target.value)}
                />
              ) : (
                <Typography.Text>{moreAbout || "NA"}</Typography.Text>
              )}
            </Col>
          </Row>
          <Row className="mtb-20">
            <Col flex="1 1 180px">
              <Typography.Text strong>Tags Followed</Typography.Text>
            </Col>
            <Col flex="1 1 180px">
              {isEdit ? (
                <Select
                  mode="multiple"
                  style={{ width: "100%" }}
                  placeholder="Select tags"
                  onChange={setCurrentSkills}
                  value={currentSkills}
                  disabled={loading}
                >
                  {availableTags.map((tag) => (
                    <Select.Option key={tag}>{tag}</Select.Option>
                  ))}
                </Select>
              ) : (
                <div>
                  {tags?.map((tag) => (
                    <Tag className="mb-10">{tag}</Tag>
                  ))}
                  {tags?.length === 0 && "NA"}
                </div>
              )}
            </Col>
          </Row>
        </Col>
        <Col flex="1 1 300px">
          <Divider orientation="left">Social</Divider>
          <div className="mtb-20 flex al-ctr">
            <div className="mr-10">
              <TwitterOutlined />
            </div>
            <div className="w350">
              {isEdit ? (
                <Input
                  placeholder="Enter Twitter Handle"
                  value={currentTwitter}
                  onChange={(e) => setCurrentTwitter(e.target.value)}
                />
              ) : twitter ? (
                <Button
                  type="link"
                  onClick={() => window.open(twitter, "_blank")}
                >
                  {twitter}
                </Button>
              ) : (
                <Button type="text">NA</Button>
              )}
            </div>
          </div>
          <div className="mtb-20 flex al-ctr">
            <div className="mr-10">
              <GithubOutlined />
            </div>
            <div className="w350">
              {isEdit ? (
                <Input
                  placeholder="Enter Github link"
                  value={currentGithub}
                  onChange={(e) => setCurrentGithub(e.target.value)}
                />
              ) : github ? (
                <Button
                  type="link"
                  onClick={() => window.open(github, "_blank")}
                >
                  {github}
                </Button>
              ) : (
                <Button type="text">NA</Button>
              )}
            </div>
          </div>
          <div className="mtb-20 flex al-ctr">
            <div className="mr-10">
              <FacebookFilled />
            </div>
            <div className="w350">
              {isEdit ? (
                <Input
                  placeholder="Enter Facebook profile"
                  value={currentFb}
                  onChange={(e) => setCurrentFb(e.target.value)}
                />
              ) : facebook ? (
                <Button
                  type="link"
                  onClick={() => window.open(facebook, "_blank")}
                >
                  {facebook}
                </Button>
              ) : (
                <Button type="text">NA</Button>
              )}
            </div>
          </div>
          <div className="mtb-20 flex al-ctr">
            <div className="mr-10">
              <LinkedinFilled />
            </div>
            <div className="w350">
              {isEdit ? (
                <Input
                  placeholder="Enter LinkenIn profile"
                  value={currentLinkedin}
                  onChange={(e) => setCurrentLinkedin(e.target.value)}
                />
              ) : linkedin ? (
                <Button
                  type="link"
                  onClick={() => window.open(linkedin, "_blank")}
                >
                  {linkedin}
                </Button>
              ) : (
                <Button type="text">NA</Button>
              )}
            </div>
          </div>
          <div className="mtb-20 flex al-ctr">
            <div className="mr-10">
              <GlobalOutlined />
            </div>
            <div className="w350">
              {isEdit ? (
                <Input
                  placeholder="Enter Website"
                  value={currentWebsite}
                  onChange={(e) => setCurrentWebsite(e.target.value)}
                />
              ) : website ? (
                <Button
                  type="link"
                  onClick={() => window.open(website, "_blank")}
                >
                  {website}
                </Button>
              ) : (
                <Button type="text">NA</Button>
              )}
            </div>
          </div>
        </Col>
      </Row>
    </ProfileWrapper>
  );
};

export default Profile;
