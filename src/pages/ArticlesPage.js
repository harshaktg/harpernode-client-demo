import { withRouter, Redirect } from "react-router-dom";
import NewArticle from "./NewArticle";
import EditArticle from "./EditArticle";

const ArticlesPage = (props) => {
  const { match, location } = props;
  if (match.params.type === "new") {
    return <NewArticle />;
  }
  if (match.params.type === "edit") {
    return <EditArticle data={location?.state?.data} />;
  }
  return <Redirect to="/notfound" />;
};

export default withRouter(ArticlesPage);
