import { useEffect, useState, useContext } from "react";
import AppContext from "contexts/AppContext";
import { Avatar, Card, Typography, Tabs, Button, message } from "antd";
import { EditOutlined, UserOutlined } from "@ant-design/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import customFetch from "utils/fetch";
import { Empty } from "assets/svg";
import ProfileSection from "../sections/Profile";
import Animation from "components/common/Animation";
import ProfileSkeleton from "components/skeletons/ProfileSkeleton";
import ProfileFollowers from "sections/ProfileFollowers";
import ProfileFollowing from "sections/ProfileFollowing";

const ProfileWrapper = styled.div`
  /* background-color: #f5f7fa; */
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  /* height: 100vh; */
  /* .ant-tabs-nav {
    margin: 0 !important;
    .ant-tabs-nav-wrap {
      justify-content: center;
    }
  } */
  .profile-card {
    width: 100%;
    &.mtb-10 {
      margin: 10px 0;
    }
    /* .ant-card-body {
      padding-bottom: 0px;
    } */
  }
  .user-content {
    padding-left: 10px;
  }
`;

const EmptyWrapper = styled.div`
  height: calc(100vh - 70px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Profile = (props) => {
  const context = useContext(AppContext);
  const { match, history } = props;

  const userId = match.params.id || context.user.user_id;

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  const [btnLoading, setBtnLoading] = useState(false);

  const getUser = async () => {
    try {
      setLoading(true);
      const res = await customFetch(`user/${userId}`, {
        method: "GET",
      });
      const { data, error } = await res.json();
      if (data) {
        setData(data);
        setError(null);
      } else if (error) {
        setError(error);
        setData(null);
      }
      setLoading(false);
    } catch (err) {
      setError(err.message);
    }
  };

  useEffect(() => {
    getUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userId]);

  const handleFollow = async () => {
    setBtnLoading(true);
    const res = await customFetch(`user/follow`, {
      method: "POST",
      body: JSON.stringify({ userId }),
    });
    const { data, error } = await res.json();
    setBtnLoading(false);
    if (data) {
      message.success(data);
    } else {
      message.error(error);
    }
    context.getUpdatedCurrentUser();
  };

  const handleUnfollow = async () => {
    setBtnLoading(true);
    const res = await customFetch(`user/unfollow`, {
      method: "POST",
      body: JSON.stringify({ userId }),
    });
    const { data, error } = await res.json();
    setBtnLoading(false);
    if (data) {
      message.success(data);
    } else {
      message.error(error);
    }
    context.getUpdatedCurrentUser();
  };

  if (error) {
    return (
      <EmptyWrapper>
        <Empty width="400" height="400" />
      </EmptyWrapper>
    );
  }

  if (loading || !data) {
    return (
      <Animation>
        <ProfileWrapper>
          <ProfileSkeleton
            buttons={userId === context?.user?.user_id ? 1 : null}
          />
        </ProfileWrapper>
      </Animation>
    );
  }

  return (
    <ProfileWrapper>
      <Animation>
        {userId === context?.user?.user_id && (
          <div className="flex mtb-10 al-ctr">
            <div className="flex al-ctr j-end f11">
              <Button
                className="mr-10"
                type="primary"
                icon={<EditOutlined />}
                size="large"
                onClick={() => history.push("/profile/edit")}
              >
                Edit
              </Button>
            </div>
          </div>
        )}
        <Card className="profile-card mtb-10">
          <div className="flex al-ctr fwrap">
            <div className="mr-20">
              <Avatar
                size={{ xs: 100, sm: 150, md: 250, lg: 250, xl: 250, xxl: 250 }}
                icon={<UserOutlined />}
                src={data.picture}
              />
            </div>
            <div>
              <Typography.Title>{data.name}</Typography.Title>
              <Typography.Title level={4} type="secondary">
                {data.tagLine}
              </Typography.Title>
              <Typography.Text type="secondary">
                {data.location}
              </Typography.Text>
              {context.user &&
                context?.user?.user_id !== userId &&
                (context.user?.following?.includes(userId) ? (
                  <div className="mtb-10">
                    <Button
                      type="default"
                      icon={
                        <FontAwesomeIcon
                          className="mr-10"
                          icon={["fas", "user-check"]}
                          size="1x"
                        />
                      }
                      loading={btnLoading}
                      onClick={handleUnfollow}
                    >
                      Following
                    </Button>
                  </div>
                ) : (
                  <div className="mtb-10">
                    <Button
                      type="primary"
                      icon={
                        <FontAwesomeIcon
                          className="mr-10"
                          icon={["fas", "user-plus"]}
                          size="1x"
                        />
                      }
                      loading={btnLoading}
                      onClick={handleFollow}
                    >
                      Follow
                    </Button>
                  </div>
                ))}
            </div>
          </div>
        </Card>
        <Card className="user-content">
          <Tabs defaultActiveKey="1" onChange={(key) => console.log(key)}>
            <Tabs.TabPane tab="Profile" key="1">
              <ProfileSection {...data} />
            </Tabs.TabPane>
            <Tabs.TabPane tab={`Followers (${data.followers?.length})`} key="2">
              <ProfileFollowers userId={userId} />
            </Tabs.TabPane>
            <Tabs.TabPane tab={`Following (${data.following?.length})`} key="3">
              <ProfileFollowing userId={userId} />
            </Tabs.TabPane>
          </Tabs>
        </Card>
      </Animation>
    </ProfileWrapper>
  );
};

export default withRouter(Profile);
