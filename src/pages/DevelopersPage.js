import { UserOutlined } from "@ant-design/icons";
import { Card, Avatar, Tag, Typography, Button } from "antd";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "utils/icon-set";

const DevelopersPageWrapper = styled.div`
  height: calc(100vh - 70px);
  overflow: auto;
  padding: 20px;
  .mb-20 {
    margin-bottom: 20px;
  }
  .w50p {
    width: 50%;
  }
  .fwrap {
    flex-wrap: wrap;
  }
`;

const SKILLS = [
  "Javascript",
  "React",
  "React Native",
  "Node JS",
  "Express JS",
  "JQuery",
  "Flutter",
  "HTML",
  "CSS",
  "SASS",
  "LESS",
  "Python",
  "Java",
  "MySQL",
  "MongoDB",
  "Webpack",
];

const SOCIAL_ITEMS = [
  {
    title: "LinkedIn",
    icon: ["fab", "linkedin"],
    link: "https://www.linkedin.com/in/harsha-vardhan-nhv/",
  },
  {
    title: "Medium",
    icon: ["fab", "medium"],
    link: "https://harshaktg.medium.com/",
  },
  {
    title: "GitLab",
    icon: ["fab", "gitlab"],
    link: "https://gitlab.com/harshaktg",
  },
  {
    title: "GitHub",
    icon: ["fab", "github"],
    link: "https://github.com/harshaktg",
  },
  {
    title: "Twitter",
    icon: ["fab", "twitter"],
    link: "https://twitter.com/nhv07",
  },
];

const CONTRIBUTORS = [
  {
    name: "Suriya Elumalai",
    designation: "Senior Software Engineer",
    avatar:
      "https://media-exp3.licdn.com/dms/image/C5103AQEEQiWKIfx6cQ/profile-displayphoto-shrink_200_200/0/1562271125974?e=1630540800&v=beta&t=x6aUm4DNu_TOTz4LeSBxctniCTldZkNehvCz2cIpbJQ",
    links: [
      {
        title: "LinkedIn",
        icon: ["fab", "linkedin"],
        link: "https://www.linkedin.com/in/suriya-elumalai-782373167/",
      },
    ],
  },
];

const DevelopersPage = (props) => {
  // const getRandomColor = () => {
  //   var letters = "0123456789ABCDEF";
  //   var color = "#";
  //   for (var i = 0; i < 6; i++) {
  //     color += letters[Math.floor(Math.random() * 16)];
  //   }
  //   return color;
  // };

  return (
    <DevelopersPageWrapper>
      <Card className="mb-20">
        <div className="flex al-ctr jc-ctr mb-20">
          <Avatar
            // size={250}
            size={{ xs: 100, sm: 150, md: 250, lg: 250, xl: 250, xxl: 250 }}
            icon={<UserOutlined />}
            src={`https://avatars.githubusercontent.com/u/40317596?v=4`}
          />
        </div>
        <div className="flex al-ctr jc-ctr ">
          <Typography.Title level={1}>Harsha Vardhan</Typography.Title>
        </div>
        <div className="flex al-ctr jc-ctr">
          <Typography.Title level={3} type="secondary">
            Senior Software Engineer
          </Typography.Title>
        </div>

        <div className="mtb-10">
          <div className="flex jc-ctr fwrap">
            <div className="mr-20">
              <Typography.Title level={4} type="secondary">
                SKILL SET
              </Typography.Title>
            </div>
            <div className="flex f11 fwrap">
              {SKILLS.map((skill) => (
                <Tag className="mb-10">{skill}</Tag>
              ))}
            </div>
          </div>
        </div>

        <div className="mtb-10">
          <div className="flex jc-ctr fwrap al-ctr">
            <div className="mr-20">
              <Typography.Title level={4} type="secondary">
                CONNECT
              </Typography.Title>
            </div>
            <div className="flex f11 fwrap">
              {SOCIAL_ITEMS.map((sc) => (
                <Button
                  className="mr-10 mb-10"
                  type="link"
                  href={sc.link}
                  target="_blank"
                  title={sc.title}
                >
                  <FontAwesomeIcon icon={sc.icon} size="lg" />
                </Button>
              ))}
            </div>
          </div>
        </div>
      </Card>
      {CONTRIBUTORS.length > 0 && (
        <>
          <Typography.Title level={2}>Contributors</Typography.Title>
          {CONTRIBUTORS.map((ctbr) => (
            <Card className="mb-20">
              <div className="flex al-ctr mb-20 fwrap">
                <div className="mr-20">
                  <Avatar
                    // size={100}
                    size={{
                      xs: 80,
                      sm: 100,
                      md: 100,
                      lg: 100,
                      xl: 100,
                      xxl: 100,
                    }}
                    icon={<UserOutlined />}
                    src={ctbr.avatar}
                  />
                </div>
                <div>
                  <Typography.Title level={2}>{ctbr.name}</Typography.Title>
                  <Typography.Title level={4} type="secondary">
                    {ctbr.designation}
                  </Typography.Title>
                </div>
              </div>
              <div className="mtb-10">
                <div className="flex jc-ctr fwrap al-ctr">
                  <div className="mr-20">
                    <Typography.Title level={4} type="secondary">
                      CONNECT
                    </Typography.Title>
                  </div>
                  <div className="flex f11 fwrap">
                    {ctbr.links.map((sc) => (
                      <Button
                        className="mr-10 mb-10"
                        type="link"
                        href={sc.link}
                        target="_blank"
                        title={sc.title}
                      >
                        <FontAwesomeIcon icon={sc.icon} size="lg" />
                      </Button>
                    ))}
                  </div>
                </div>
              </div>
            </Card>
          ))}
        </>
      )}
    </DevelopersPageWrapper>
  );
};

export default DevelopersPage;
