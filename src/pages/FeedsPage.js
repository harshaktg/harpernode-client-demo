import { useState, useEffect, useContext } from "react";
import { Card, Typography } from "antd";
import customFetch from "utils/fetch";
import { Empty } from "assets/svg";
import Animation from "components/common/Animation";
import PostCard from "components/common/PostCard";
import styled from "styled-components";
import PostCardSkeleton from "components/skeletons/PostCardSkeleton";
import AppContext from "contexts/AppContext";

const FeedsPageWrapper = styled.div`
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  .header-card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w50p {
    width: 50%;
  }
`;

const EmptyWrapper = styled.div`
  height: calc(100vh - 140px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const FeedsPage = (props) => {
  const context = useContext(AppContext);
  const [loading, setLoading] = useState(false);
  const [posts, setPosts] = useState([]);

  const getFeed = async () => {
    setLoading(true);
    const res = await customFetch(`feed`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setPosts(data);
  };

  const getAllPosts = async () => {
    setLoading(true);
    const res = await customFetch(`posts`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setPosts(data);
  };

  useEffect(() => {
    if (context.user?.user_id) {
      getFeed();
    } else {
      getAllPosts();
    }
  }, [context.user]);

  if (loading) {
    const total = Array.from({ length: 3 }, () => 0);
    return (
      <Animation>
        <FeedsPageWrapper>
          {total.map((crd) => (
            <PostCardSkeleton />
          ))}
        </FeedsPageWrapper>
      </Animation>
    );
  }

  return (
    <FeedsPageWrapper>
      {context.user && (
        <Animation>
          <Card>
            <Typography.Title level={1}>
              Good day, {context.user?.name}!
            </Typography.Title>
            <Typography.Paragraph type="secondary">
              Customize your feed by following users or by following tags in the
              Profile section
            </Typography.Paragraph>
          </Card>
        </Animation>
      )}
      {posts?.map((post) => (
        <Animation>
          <PostCard data={post} />
        </Animation>
      ))}
      {posts?.length === 0 && (
        <Animation>
          <EmptyWrapper>
            <Empty width="300" height="300" />
          </EmptyWrapper>
        </Animation>
      )}
    </FeedsPageWrapper>
  );
};

export default FeedsPage;
