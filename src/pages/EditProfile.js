import { useState, useEffect, useContext } from "react";
import { Avatar, Card, Tabs, Input, Button, message } from "antd";
import { withRouter } from "react-router-dom";
import customFetch from "utils/fetch";
import { UserOutlined, SaveOutlined } from "@ant-design/icons";
import styled from "styled-components";
import Animation from "components/common/Animation";
import ProfileSkeleton from "components/skeletons/ProfileSkeleton";
import ProfileSection from "../sections/Profile";
import AppContext from "contexts/AppContext";

const ProfileWrapper = styled.div`
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  .profile-name {
    font-size: 38px;
    /* border: none !important;
    box-shadow: none !important; */
  }
  .profile-tagline {
    font-size: 20px;
    margin: 10px 0;
  }
  .profile-card {
    width: 100%;
    &.mtb-10 {
      margin: 10px 0;
    }
    /* .ant-card-body {
      padding-bottom: 0px;
    } */
  }
  .user-content {
    padding-left: 10px;
  }
`;

const EditProfile = (props) => {
  const context = useContext(AppContext);

  const [currentName, setCurrentName] = useState("");
  const [currentTagLine, setCurrentTagLine] = useState("");
  const [currentLocation, setCurrentLocation] = useState("");
  const [currentAvailableFor, setCurrentAvailableFor] = useState("");
  const [currentMoreAbout, setCurrentMoreAbout] = useState("");
  const [currentSkills, setCurrentSkills] = useState([]);
  const [currentTwitter, setCurrentTwitter] = useState("");
  const [currentGithub, setCurrentGithub] = useState("");
  const [currentFb, setCurrentFb] = useState("");
  const [currentLinkedin, setCurrentLinkedin] = useState("");
  const [currentWebsite, setCurrentWebsite] = useState("");
  const [picture, setPicture] = useState(null);

  const [loading, setLoading] = useState(false);

  const setAllVariables = (data) => {
    const {
      name = "",
      tagLine = "",
      location = "",
      availableFor = "",
      moreAbout = "",
      tags = [],
      twitter = "",
      github = "",
      facebook = "",
      linkedin = "",
      website = "",
      picture,
    } = data;
    setCurrentName(name);
    setCurrentTagLine(tagLine);
    setCurrentLocation(location);
    setCurrentAvailableFor(availableFor);
    setCurrentMoreAbout(moreAbout);
    setCurrentSkills(tags);
    setCurrentTwitter(twitter);
    setCurrentGithub(github);
    setCurrentLinkedin(linkedin);
    setCurrentFb(facebook);
    setCurrentWebsite(website);
    setPicture(picture);
  };

  const getCurrentUser = async () => {
    setLoading(true);
    const res = await customFetch(`currentUser`, {
      method: "GET",
    });
    const data = await res.json();
    setLoading(false);
    setAllVariables(data);
  };

  useEffect(() => {
    getCurrentUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const stateObj = {
    currentAvailableFor,
    setCurrentAvailableFor,
    currentMoreAbout,
    setCurrentMoreAbout,
    currentSkills,
    setCurrentSkills,
    currentTwitter,
    setCurrentTwitter,
    currentGithub,
    setCurrentGithub,
    currentFb,
    setCurrentFb,
    currentLinkedin,
    setCurrentLinkedin,
    currentWebsite,
    setCurrentWebsite,
  };

  const handleSave = async () => {
    const requestOptions = {
      name: currentName,
      tagLine: currentTagLine,
      location: currentLocation,
      availableFor: currentAvailableFor,
      moreAbout: currentMoreAbout,
      tags: currentSkills,
      twitter: currentTwitter,
      github: currentGithub,
      facebook: currentFb,
      linkedin: currentLinkedin,
      website: currentWebsite,
    };
    setLoading(true);
    const res = await customFetch(`user`, {
      method: "PATCH",
      body: JSON.stringify(requestOptions),
    });
    const { data, error } = await res.json();
    setLoading(false);
    context.getUpdatedCurrentUser();
    if (error) {
      message.error(error);
      props.history.push("/profile/edit");
    } else {
      message.success(data);
      props.history.push("/profile/view");
    }
  };

  if (loading) {
    return (
      <Animation>
        <ProfileWrapper>
          <ProfileSkeleton buttons={2} />
        </ProfileWrapper>
      </Animation>
    );
  }

  return (
    <ProfileWrapper>
      <Animation>
        <div className="flex mtb-10 al-ctr">
          <div className="flex al-ctr j-end f11">
            <Button
              className="mr-10"
              type="text"
              size="large"
              onClick={() => props.history.push("/profile/view")}
            >
              Cancel
            </Button>
            <Button
              className="mr-10"
              type="primary"
              icon={<SaveOutlined />}
              size="large"
              onClick={handleSave}
            >
              Save
            </Button>
          </div>
        </div>
        <Card className="profile-card mtb-10">
          <div className="flex al-ctr">
            <div className="mr-20">
              <Avatar size={150} icon={<UserOutlined />} src={picture} />
            </div>
            <div>
              <Input
                placeholder="Enter name"
                className="profile-name"
                value={currentName}
                onChange={(e) => setCurrentName(e.target.value)}
              />
              <Input
                placeholder="Enter profile tag line"
                className="profile-tagline"
                value={currentTagLine}
                onChange={(e) => setCurrentTagLine(e.target.value)}
              />
              <Input
                placeholder="Chennai, India"
                value={currentLocation}
                onChange={(e) => setCurrentLocation(e.target.value)}
              />
            </div>
          </div>
        </Card>
        <Card className="user-content">
          <Tabs defaultActiveKey="1" onChange={(key) => console.log(key)}>
            <Tabs.TabPane tab="Profile" key="1">
              <ProfileSection isEdit {...stateObj} />
            </Tabs.TabPane>
          </Tabs>
        </Card>
      </Animation>
    </ProfileWrapper>
  );
};

export default withRouter(EditProfile);
