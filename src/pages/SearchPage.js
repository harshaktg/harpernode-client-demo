import { useState } from "react";
import { Card, Input, Typography, message } from "antd";
import { isMobile } from "react-device-detect";
import styled from "styled-components";
import { Search, Empty } from "assets/svg";
import customFetch from "utils/fetch";
import Animation from "components/common/Animation";
import PostCard from "components/common/PostCard";
import UserCard from "components/common/UserCard";

const SearchWrapper = styled.div`
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  .header-card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w50p {
    width: 50%;
  }
  .w100p {
    width: 50%;
  }
  .ow-any {
    overflow-wrap: anywhere;
  }
`;

const SearchImageWrapper = styled.div`
  height: calc(100vh - 300px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SearchPage = (props) => {
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);
  const [searchKey, setSearchKey] = useState("");
  const [searchLoading, setSearchLoading] = useState(false);

  const onSearchHandler = async () => {
    if (!searchKey.trim()) {
      message.error("Please enter any search term");
      return null;
    }
    setSearchLoading(true);
    const res = await customFetch(`search/?searchKey=${searchKey}`, {
      method: "GET",
    });
    const { data } = await res.json();
    setSearchLoading(false);
    if (data) {
      setPosts(data?.posts || []);
      setUsers(data?.users || []);
    }
  };

  const SearchLoader = () => {
    return (
      <Animation>
        <SearchImageWrapper>
          <Search width="300" height="300" />
        </SearchImageWrapper>
      </Animation>
    );
  };

  return (
    <SearchWrapper>
      <Card>
        <div className="header-card">
          <div>
            <Typography.Title className="ow-any">
              HarperNode Search
            </Typography.Title>
          </div>
          <div>
            Search for any User, Post or anything. Let's see what we can find
            for you! :)
          </div>
          <div className={isMobile ? "flex mt-20 w100p" : "flex mt-20 w50p"}>
            <Input.Search
              placeholder="Search here"
              loading={searchLoading}
              value={searchKey}
              onChange={(e) => setSearchKey(e.target.value)}
              enterButton
              onPressEnter={onSearchHandler}
              onSearch={onSearchHandler}
            />
          </div>
        </div>
      </Card>
      {searchLoading ? (
        <SearchLoader />
      ) : (
        <>
          {users?.length > 0 && (
            <div>
              <Typography.Title level={2}>All Users</Typography.Title>
            </div>
          )}
          {users?.map((user) => (
            <Animation>
              <UserCard data={user} />
            </Animation>
          ))}
          {posts?.length > 0 && (
            <div>
              <Typography.Title level={2}>All Posts</Typography.Title>
            </div>
          )}
          {posts?.map((post) => (
            <Animation>
              <PostCard data={post} />
            </Animation>
          ))}
          {posts?.length === 0 && users?.length === 0 && (
            <Animation>
              <SearchImageWrapper>
                <Empty width="300" height="300" />
              </SearchImageWrapper>
            </Animation>
          )}
        </>
      )}
    </SearchWrapper>
  );
};

export default SearchPage;
