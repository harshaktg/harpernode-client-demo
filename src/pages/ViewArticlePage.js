import { useState, useEffect } from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import { withRouter } from "react-router-dom";
import Animation from "components/common/Animation";
import customFetch from "utils/fetch";
import { Empty } from "assets/svg";
import ArticleDetails from "sections/ArticleDetails";
import ArticleContent from "sections/ArticleContent";
import ViewArticleSkeleton from "components/skeletons/ViewArticleSkeleton";

const EmptyWrapper = styled.div`
  height: calc(100vh - 70px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ViewArticleWrapper = styled.div`
  height: calc(100vh - 70px);
  padding: 20px;
  overflow: auto;
`;

const ViewArticlePage = (props) => {
  const { match } = props;

  const postId = match.params.id;

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  const getPost = async () => {
    try {
      setLoading(true);
      const res = await customFetch(`posts/${postId}`, {
        method: "GET",
      });
      const { data, error } = await res.json();
      if (data) {
        setData(data);
        setError(null);
      } else if (error) {
        setError(error);
        setData(null);
      }
      setLoading(false);
    } catch (err) {
      setError(err.message);
    }
  };

  useEffect(() => {
    getPost();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (error) {
    return (
      <EmptyWrapper>
        <Empty width="400" height="400" />
      </EmptyWrapper>
    );
  }

  if (loading || !data) {
    return (
      <Animation>
        <ViewArticleWrapper>
          <ViewArticleSkeleton />
        </ViewArticleWrapper>
      </Animation>
    );
  }

  return (
    <ViewArticleWrapper>
      <Animation>
        <Row gutter="16">
          <Col className="gutter-row" span={16}>
            <ArticleContent data={data} />
          </Col>
          <Col className="gutter-row" span={8}>
            <ArticleDetails data={data} />
          </Col>
        </Row>
      </Animation>
    </ViewArticleWrapper>
  );
};

export default withRouter(ViewArticlePage);
