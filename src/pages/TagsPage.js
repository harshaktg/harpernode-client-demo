import { useState, useEffect } from "react";
import { Card, Select, Typography, Button } from "antd";
import { isMobile } from "react-device-detect";
import { SearchOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { TagSearch } from "assets/svg";
import customFetch from "utils/fetch";
import Animation from "components/common/Animation";
import PostCard from "components/common/PostCard";

const TagsWrapper = styled.div`
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  .header-card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w50p {
    width: 50%;
  }
  .ow-any {
    overflow-wrap: anywhere;
  }
  .w100p {
    width: 100%;
  }
  .mt-10 {
    margin-top: 10px !important;
  }
`;

const TagSearchWrapper = styled.div`
  height: calc(100vh - 300px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const TagsPage = (props) => {
  const [availableTags, setAvailableTags] = useState([]);
  const [loading, setLoading] = useState(false);
  const [posts, setPosts] = useState([]);
  const [searchLoading, setSearchLoading] = useState(false);
  const [selectedTags, setSelectedTags] = useState([]);

  const getAllTags = async () => {
    setLoading(true);
    const res = await customFetch(`tags`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setAvailableTags(data.map((tg) => tg.name));
  };

  useEffect(() => {
    getAllTags();
  }, []);

  const onSearchHandler = async () => {
    if (!selectedTags.length) {
      return null;
    }
    setSearchLoading(true);
    const res = await customFetch(`posts/tags`, {
      method: "POST",
      body: JSON.stringify({ tags: selectedTags }),
    });
    const { data } = await res.json();
    setSearchLoading(false);
    setPosts(data);
  };

  return (
    <TagsWrapper>
      <Card>
        <div className="header-card">
          <div>
            <Typography.Title className="ow-any">
              HarperNode Tag Search
            </Typography.Title>
          </div>
          <div>Select one or more tags to show articles based on them</div>
          <div className={isMobile ? "mt-20 w100p" : "flex mt-20 w50p"}>
            <Select
              mode="multiple"
              style={{ width: "100%" }}
              placeholder="Select tags"
              onChange={setSelectedTags}
              value={selectedTags}
              disabled={loading || searchLoading}
              className="mr-10"
            >
              {availableTags.map((tag) => (
                <Select.Option key={tag}>{tag}</Select.Option>
              ))}
            </Select>
            <Button
              type="primary"
              shape={!isMobile && "circle"}
              icon={<SearchOutlined />}
              size="medium"
              className={isMobile && "mt-10 w100p"}
              disabled={loading}
              loading={searchLoading}
              onClick={onSearchHandler}
            >
              {isMobile && "Search"}
            </Button>
          </div>
        </div>
      </Card>
      {posts?.map((post) => (
        <Animation>
          <PostCard data={post} />
        </Animation>
      ))}
      {posts?.length === 0 && (
        <Animation>
          <TagSearchWrapper>
            <TagSearch width="300" height="300" />
          </TagSearchWrapper>
        </Animation>
      )}
    </TagsWrapper>
  );
};

export default TagsPage;
