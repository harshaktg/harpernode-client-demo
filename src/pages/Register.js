import { useContext } from "react";
import styled from "styled-components";
import { isMobile } from "react-device-detect";
import { Button, Card, Typography } from "antd";
import { HarperNode } from "assets/svg";
import GoogleLogin from "react-google-login";
import { Redirect } from "react-router-dom";
import AppContext from "contexts/AppContext";

const RegisterWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 70px);
  .illustration-wrapper {
    border-right: 1px solid #e6ebf1;
  }
  .fvert {
    flex-direction: column;
  }
  .connect-desc {
    font-size: 18px;
    font-weight: 700;
    margin: 10px 0;
  }
  .app-title {
    font-size: 48px;
    font-weight: 500;
    margin-left: 10px;
  }
  .register-container {
    /* margin-left: 100px; */
    padding: ${(props) => (props.isMobile ? "" : `60px`)};
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  .social-wrapper {
    margin-bottom: 10px;
  }
  .ant-btn {
    height: initial;
    width: 150px;
    .social-logo {
      width: 20px;
      height: 20px;
    }
    .social-name {
      margin-left: 10px;
      font-weight: 500;
      font-size: 16px;
    }
  }
  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(359deg);
    }
  }
  .logo-spin {
    animation: spin 400ms linear forwards;
  }
`;

const Register = (props) => {
  const context = useContext(AppContext);

  if (context.user) {
    return <Redirect to="/" />;
  }

  return (
    <RegisterWrapper isMobile={isMobile}>
      {/* <SignIn width="450px" height="100%" /> */}
      <Card>
        <div className="register-container">
          <div className={isMobile ? "flex al-ctr fvert" : "flex al-ctr"}>
            <HarperNode width="80" height="80" className="logo-spin" />
            <div className="app-title">HarperNode</div>
          </div>
          <div className="mtb-10">
            <Typography.Paragraph>
              Join us to write articles and connect with the Dev Community
            </Typography.Paragraph>
          </div>
          <div className="connect-desc">Connect Using:</div>
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
            disabled={context.user}
            render={(renderProps) => (
              <div className="social-wrapper">
                <Button
                  onClick={renderProps.onClick}
                  disabled={renderProps.disabled}
                >
                  <div className="flex al-ctr">
                    <div>
                      <img
                        src={`${process.env.PUBLIC_URL}/img/google.png`}
                        alt="Google"
                        className="social-logo"
                      />
                    </div>
                    <div className="social-name">Google</div>
                  </div>
                </Button>
              </div>
            )}
            buttonText="Log in with Google"
            onSuccess={context.googleLogIn}
            onFailure={(err) => console.log(err)}
            cookiePolicy={"single_host_origin"}
          />
          {/* {SOCIAL_LOGINS.map((type) => (
          <div className="social-wrapper">
            <Button onClick={onRegisterHandler}>
              <div className="flex al-ctr">
                <div>
                  <img
                    src={`${process.env.PUBLIC_URL}/img/${type.logo}`}
                    alt={type.name}
                    className="social-logo"
                  />
                </div>
                <div className="social-name">{type.name}</div>
              </div>
            </Button>
          </div>
        ))} */}
        </div>
      </Card>
    </RegisterWrapper>
  );
};

export default Register;
