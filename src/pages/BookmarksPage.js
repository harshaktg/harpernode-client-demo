import { useState, useEffect, useContext } from "react";
import { Card, Typography } from "antd";
import customFetch from "utils/fetch";
import { Empty } from "assets/svg";
import Animation from "components/common/Animation";
import PostCard from "components/common/PostCard";
import styled from "styled-components";
import PostCardSkeleton from "components/skeletons/PostCardSkeleton";
import AppContext from "contexts/AppContext";

const BookmarksPageWrapper = styled.div`
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  .header-card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w50p {
    width: 50%;
  }
`;

const EmptyWrapper = styled.div`
  height: calc(100vh - 140px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const BookmarksPage = (props) => {
  const context = useContext(AppContext);

  const [loading, setLoading] = useState(false);
  const [posts, setPosts] = useState([]);

  const getBookmarks = async () => {
    setLoading(true);
    const res = await customFetch(`bookmarks`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setPosts(data);
  };

  useEffect(() => {
    getBookmarks();
  }, []);

  if (loading) {
    const total = Array.from({ length: 3 }, () => 0);
    return (
      <Animation>
        <BookmarksPageWrapper>
          {total.map((crd) => (
            <PostCardSkeleton />
          ))}
        </BookmarksPageWrapper>
      </Animation>
    );
  }

  return (
    <BookmarksPageWrapper>
      {context.user && (
        <Animation>
          <Card>
            <Typography.Title level={1}>Bookmarks</Typography.Title>
            <Typography.Paragraph type="secondary">
              Here are the list of your bookmarked posts
            </Typography.Paragraph>
          </Card>
        </Animation>
      )}
      {posts?.map((post) => (
        <Animation>
          <PostCard data={post} />
        </Animation>
      ))}
      {posts?.length === 0 && (
        <Animation>
          <EmptyWrapper>
            <Empty width="300" height="300" />
          </EmptyWrapper>
        </Animation>
      )}
    </BookmarksPageWrapper>
  );
};

export default BookmarksPage;
