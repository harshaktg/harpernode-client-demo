import { useState, useEffect, useContext } from "react";
import customFetch from "utils/fetch";
import { Empty } from "assets/svg";
import Animation from "components/common/Animation";
import PostCard from "components/common/PostCard";
import styled from "styled-components";
import PostCardSkeleton from "components/skeletons/PostCardSkeleton";
import AppContext from "contexts/AppContext";

const MyPostsPageWrapper = styled.div`
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  .header-card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w50p {
    width: 50%;
  }
`;

const EmptyWrapper = styled.div`
  height: calc(100vh - 140px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const MyPostsPage = (props) => {
  const context = useContext(AppContext);
  const [loading, setLoading] = useState(false);
  const [posts, setPosts] = useState([]);

  const getMyPosts = async () => {
    setLoading(true);
    const res = await customFetch(`posts/myPosts`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setPosts(data);
  };

  useEffect(() => {
    getMyPosts();
  }, [context.user]);

  if (loading) {
    const total = Array.from({ length: 3 }, () => 0);
    return (
      <Animation>
        <MyPostsPageWrapper>
          {total.map((crd) => (
            <PostCardSkeleton />
          ))}
        </MyPostsPageWrapper>
      </Animation>
    );
  }

  return (
    <MyPostsPageWrapper>
      {posts?.map((post) => (
        <Animation>
          <PostCard data={post} triggerReload={getMyPosts} />
        </Animation>
      ))}
      {posts?.length === 0 && (
        <Animation>
          <EmptyWrapper>
            <Empty width="300" height="300" />
          </EmptyWrapper>
        </Animation>
      )}
    </MyPostsPageWrapper>
  );
};

export default MyPostsPage;
