import { useState } from "react";
import { message } from "antd";
import { withRouter } from "react-router-dom";
import ArticleSheet from "../sheets/ArticleSheet";
import customFetch from "utils/fetch";

const NewArticle = (props) => {
  const { history } = props;
  const [loading, setLoading] = useState(false);

  const createArticle = async (obj) => {
    if (!obj.title.trim()) {
      message.error("Title is required");
      return null;
    }
    if (!obj.content.trim()) {
      message.error("Content is required");
      return null;
    }
    setLoading(true);
    const res = await customFetch(`posts`, {
      method: "POST",
      body: JSON.stringify(obj),
    });
    const { data, error } = await res.json();
    setLoading(false);
    if (data) {
      message.success(data);
      history.push("/myposts");
    } else {
      message.error(error);
    }
  };

  return (
    <div>
      <ArticleSheet action={createArticle} submitLoading={loading} />
    </div>
  );
};

export default withRouter(NewArticle);
