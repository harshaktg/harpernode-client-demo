import { Card, Typography, Row, Col, List, Avatar, Skeleton, Tag } from "antd";
import { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import Animation from "components/common/Animation";
import customFetch from "utils/fetch";
import styled from "styled-components";

const ExplorePageWrapper = styled.div`
  padding: 20px;
  height: calc(100vh - 70px);
  overflow: auto;
  .header-card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w50p {
    width: 50%;
  }
  .list-item {
    cursor: pointer;
    height: 80px;
  }
`;

const ExplorePage = (props) => {
  const [usersLoading, setUsersLoading] = useState(false);
  const [postsLoading, setPostsLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [posts, setPosts] = useState([]);

  const getUsers = async () => {
    setUsersLoading(true);
    const res = await customFetch(`user/topUsers`, {
      method: "GET",
    });
    const { data } = await res.json();
    setUsersLoading(false);
    setUsers(data);
  };

  const getPosts = async () => {
    setPostsLoading(true);
    const res = await customFetch(`posts/topPosts`, {
      method: "GET",
    });
    const { data } = await res.json();
    setPostsLoading(false);
    setPosts(data);
  };

  useEffect(() => {
    getUsers();
    getPosts();
  }, []);

  const loadSkeletons = (length) => {
    const total = Array.from({ length }, () => 0);

    return (
      <>
        {total.map((btn) => (
          <Skeleton
            active
            avatar
            loading={usersLoading}
            paragraph={{ rows: 1 }}
          />
        ))}
      </>
    );
  };

  return (
    <ExplorePageWrapper>
      <Animation>
        <Row gutter="16">
          {/* <Col span={12}> */}
          <Col flex="1 1 300px">
            <Card>
              <Typography.Title level={4}>Top Users</Typography.Title>
              {usersLoading ? (
                loadSkeletons(5)
              ) : (
                <Animation>
                  <List
                    itemLayout="horizontal"
                    dataSource={users}
                    renderItem={(item) => (
                      <List.Item
                        className="list-item"
                        onClick={() =>
                          props.history.push(`/profile/view/${item.user_id}`)
                        }
                      >
                        <List.Item.Meta
                          avatar={<Avatar src={item.picture} />}
                          title={
                            <Typography.Title level={4} ellipsis>
                              <Link to={`/profile/view/${item.user_id}`}>
                                {item.name}
                              </Link>
                            </Typography.Title>
                          }
                          description={item.tagLine}
                        />
                      </List.Item>
                    )}
                  />
                </Animation>
              )}
            </Card>
          </Col>
          {/* <Col span={12}> */}
          <Col flex="1 1 300px">
            <Card>
              <Typography.Title level={4}>Trending Posts</Typography.Title>
              {postsLoading ? (
                loadSkeletons(5)
              ) : (
                <Animation>
                  <List
                    itemLayout="horizontal"
                    dataSource={posts}
                    renderItem={(item) => (
                      <List.Item
                        className="list-item"
                        onClick={() =>
                          props.history.push(`/article/view/${item.post_id}`)
                        }
                      >
                        <List.Item.Meta
                          title={
                            <Typography.Title level={4} ellipsis>
                              <Link to={`/article/view/${item.post_id}`}>
                                {item.title}
                              </Link>
                            </Typography.Title>
                          }
                          description={
                            <>
                              {item.tags.map((it) => (
                                <Tag>{it}</Tag>
                              ))}
                            </>
                          }
                        />
                      </List.Item>
                    )}
                  />
                </Animation>
              )}
            </Card>
          </Col>
        </Row>
      </Animation>
    </ExplorePageWrapper>
  );
};

export default withRouter(ExplorePage);
