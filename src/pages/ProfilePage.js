import { withRouter, Redirect } from "react-router-dom";
import Profile from "./Profile";
import EditProfile from "./EditProfile";

const ProfilePage = (props) => {
  const { match } = props;
  if (match.params.type === "view") {
    return <Profile />;
  }
  if (match.params.type === "edit") {
    return <EditProfile />;
  }
  return <Redirect to="/notfound" />;
};

export default withRouter(ProfilePage);
