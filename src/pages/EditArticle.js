import ArticleSheet from "../sheets/ArticleSheet";
import { useContext, useState } from "react";
import { message } from "antd";
import { Redirect, withRouter } from "react-router-dom";
import AppContext from "contexts/AppContext";
import customFetch from "utils/fetch";

const EditArticle = (props) => {
  const context = useContext(AppContext);
  const { history } = props;
  const [loading, setLoading] = useState(false);

  if (!props?.data) {
    return <Redirect to={`/`} />;
  }
  if (props.data.created_user?.user_id !== context?.user?.user_id) {
    return <Redirect to={`/article/view/${props.data.post_id}`} />;
  }

  const updateArticle = async (obj) => {
    if (!obj.title.trim()) {
      message.error("Title is required");
      return null;
    }
    if (!obj.content.trim()) {
      message.error("Content is required");
      return null;
    }
    setLoading(true);
    const res = await customFetch(`posts/${props.data.post_id}`, {
      method: "POST",
      body: JSON.stringify(obj),
    });
    const { data, error } = await res.json();
    setLoading(false);
    if (data) {
      message.success(data);
      history.push("/myposts");
    } else {
      message.error(error);
    }
  };

  return (
    <div>
      <ArticleSheet
        {...props.data}
        action={updateArticle}
        submitLoading={loading}
      />
    </div>
  );
};

export default withRouter(EditArticle);
