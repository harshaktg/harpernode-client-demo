import { useState, useEffect, useContext } from "react";
import { isBrowser, isMobile } from "react-device-detect";
import { Layout, Menu } from "antd";
import { MENU_ITEMS, OTHER_LINKS } from "constants/index";
import { withRouter } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "utils/icon-set";
import AppContext from "contexts/AppContext";

const { Sider } = Layout;

const SideBar = (props) => {
  const context = useContext(AppContext);

  const [collapsed, setCollaped] = useState(false);
  const [selectedItems, setSelectedItems] = useState(null);

  useEffect(() => {
    if (props.location.pathname) {
      setSelectedItems([props.location.pathname.slice(1)]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleMenuItemClick = ({ key }) => {
    props.history.push(`/${key}`);
    setSelectedItems([key]);
  };

  const authorisedItems = MENU_ITEMS.filter((item) => {
    if (!item.secure) {
      return true;
    }
    if (context.user && item.secure) {
      return true;
    }
    return false;
  });

  return (
    <Sider
      breakpoint="md"
      collapsedWidth={isMobile ? 0 : 75}
      width="230"
      className="app-sidebar"
      collapsible
      collapsed={collapsed}
      onCollapse={() => setCollaped(!collapsed)}
      trigger={
        collapsed ? (
          <div className="flex al-ctr jc-ctr">
            <div>
              <FontAwesomeIcon
                className="mlr-10"
                icon={["fas", "angle-double-right"]}
                size="1x"
              />
            </div>
          </div>
        ) : (
          <div className="flex al-ctr jc-ctr">
            <div>
              <FontAwesomeIcon
                className="mlr-10"
                icon={["fas", "angle-double-left"]}
                size="1x"
              />
            </div>
            {isBrowser && <div>Collapse sidebar</div>}
          </div>
        )
      }
    >
      <Menu selectedKeys={selectedItems} mode="inline">
        {authorisedItems.map((item) => (
          <Menu.Item
            key={item.key}
            icon={<FontAwesomeIcon icon={item.icon} size="1x" />}
            onClick={handleMenuItemClick}
          >
            {item.name}
          </Menu.Item>
        ))}
        <Menu.SubMenu
          key="more"
          icon={<FontAwesomeIcon icon={["fas", "ellipsis-v"]} />}
          size="1x"
          title="More"
        >
          {OTHER_LINKS.map((item) => (
            <Menu.Item
              key={item.key}
              icon={<FontAwesomeIcon icon={item.icon} size="1x" />}
              onClick={handleMenuItemClick}
            >
              {item.name}
            </Menu.Item>
          ))}
          {/* <Menu.Item
            key="developers"
            icon={<FontAwesomeIcon icon={["fas", "theater-masks"]} />}
            size="1x"
          >
            Developers
          </Menu.Item>
          <Menu.Item
            key="aboutus"
            icon={<FontAwesomeIcon icon={["fas", "info-circle"]} />}
            size="1x"
          >
            About Us
          </Menu.Item> */}
        </Menu.SubMenu>
      </Menu>
    </Sider>
  );
};

export default withRouter(SideBar);
