import { useContext, useState } from "react";
import { useThemeSwitcher } from "react-css-theme-switcher";
import { Layout, Button, Tooltip, Popover, Avatar, Modal } from "antd";
import { isMobile } from "react-device-detect";
import { CloseCircleOutlined } from "@ant-design/icons";
import Logo from "components/common/Logo";
import { nanoid } from "nanoid";
import { withRouter, Link } from "react-router-dom";
import AppContext from "contexts/AppContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import customFetch from "utils/fetch";
import styled from "styled-components";
import "utils/icon-set";

const { Header } = Layout;

const SettingsMenuWrapper = styled.div`
  .flex {
    display: flex;
  }
  .al-ctr {
    align-items: center;
  }
  .mr-10 {
    margin-right: 10px;
  }
  .menu-item {
    padding: 10px 0;
    .edit-profile {
      color: inherit;
      &:hover {
        color: #3e79f7;
      }
    }
    cursor: pointer;
    transition: color 0.3s ease 0s;
    &:hover {
      color: #3e79f7;
      /* color: #ff6b72; */
    }
  }
`;

const SettingsMenu = (props) => {
  const context = useContext(AppContext);

  const deleteAccount = async () => {
    const res = await customFetch(`user`, {
      method: "DELETE",
    });
    await res.json();
    context.getCurrentUser();
  };

  const showDeleteConfirm = () => {
    Modal.error({
      title: "Are you sure you want to delete your account?",
      icon: <CloseCircleOutlined />,
      content: "Your posts will still be here. You can come back anytime :)",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        deleteAccount();
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const handleSignOut = () => {
    props.hideMenu();
    context.logOut();
  };

  const handleDeleteAccount = () => {
    props.hideMenu();
    showDeleteConfirm();
  };

  return (
    <SettingsMenuWrapper>
      {isMobile && (
        <>
          <div className="menu-item flex al-ctr jc-ctr">
            <Button
              type="primary"
              onClick={() => props.history.push(`/article/new/${nanoid()}`)}
              className="mr-10"
            >
              Write now
            </Button>
          </div>
          <div className="menu-item" onClick={props.hideMenu}>
            <Link to="/profile/view" className="edit-profile">
              <div className="flex al-ctr">
                <div>
                  <FontAwesomeIcon
                    className="mr-10"
                    icon={["fas", "user"]}
                    size="lg"
                  />
                </div>
                <div>View Profile</div>
              </div>
            </Link>
          </div>
        </>
      )}
      <div className="menu-item" onClick={props.hideMenu}>
        <Link to="/profile/edit" className="edit-profile">
          <div className="flex al-ctr">
            <div>
              <FontAwesomeIcon
                className="mr-10"
                icon={["fas", "user-edit"]}
                size="lg"
              />
            </div>
            <div>Edit Profile</div>
          </div>
        </Link>
      </div>
      <div className="menu-item" onClick={handleSignOut}>
        <div className="flex al-ctr">
          <div>
            <FontAwesomeIcon
              className="mr-10"
              icon={["fas", "sign-out-alt"]}
              size="lg"
            />
          </div>
          <div>Log out</div>
        </div>
      </div>
      <div className="menu-item" onClick={handleDeleteAccount}>
        <div className="flex al-ctr">
          <div>
            <FontAwesomeIcon
              className="mr-10"
              icon={["fas", "user-times"]}
              size="lg"
            />
          </div>
          <div>Delete account</div>
        </div>
      </div>
    </SettingsMenuWrapper>
  );
};

const TopBar = (props) => {
  const { history } = props;
  const context = useContext(AppContext);

  const [menuOpen, setMenuOpen] = useState(false);

  const { switcher, currentTheme, themes } = useThemeSwitcher();

  const toggleTheme = (isChecked) => {
    if (isChecked) {
      localStorage.setItem("harperNodeTheme", "dark");
    } else {
      localStorage.setItem("harperNodeTheme", "light");
    }
    switcher({ theme: isChecked ? themes.dark : themes.light });
  };

  // Avoid theme change flicker
  // if (status === "loading") {
  //   return null;
  // }

  return (
    <Header className="app-header sp-btwn">
      <Logo />
      <div className="flex al-ctr">
        {!isMobile && context.user && (
          <Button
            type="primary"
            onClick={() => props.history.push(`/article/new/${nanoid()}`)}
            className="mr-10"
          >
            Write now
          </Button>
        )}
        {!isMobile && context.user && (
          <Button
            type="text"
            onClick={() => props.history.push("/profile/view")}
            className="prof-btn mr-20"
          >
            <div className="flex al-ctr prof-name">
              <div className="mr-10">{context.user.name}</div>
              <div>
                <Avatar src={context.user.picture} />
              </div>
            </div>
          </Button>
        )}
        <div
          className="theme-switcher"
          onClick={() => toggleTheme(currentTheme !== "dark")}
        >
          <Tooltip title="Toggle theme">
            {currentTheme === "dark" ? (
              <FontAwesomeIcon icon={["fas", "sun"]} size="lg" />
            ) : (
              <FontAwesomeIcon icon={["fas", "moon"]} size="lg" />
            )}
          </Tooltip>
        </div>
        <div className="mlr-20">
          {!context.user && (
            <Button type="primary" onClick={() => history.push("/register")}>
              Sign In
            </Button>
          )}
          {context.user && (
            <Tooltip title="Settings">
              <Popover
                placement="bottomRight"
                content={() => (
                  <SettingsMenu
                    hideMenu={() => setMenuOpen(false)}
                    history={history}
                  />
                )}
                trigger="click"
                visible={menuOpen}
                onVisibleChange={() => setMenuOpen(!menuOpen)}
              >
                <Button type="text">
                  <FontAwesomeIcon icon={["fas", "cog"]} size="lg" />
                </Button>
              </Popover>
            </Tooltip>
          )}
        </div>
      </div>
    </Header>
  );
};

export default withRouter(TopBar);
