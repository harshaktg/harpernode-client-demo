import { useState, useEffect } from "react";
import {
  Drawer,
  Select,
  Button,
  Tooltip,
  Typography,
  Space,
  Modal,
} from "antd";
import { isMobile } from "react-device-detect";
import { withRouter } from "react-router-dom";
import { SettingOutlined, SendOutlined, SaveOutlined } from "@ant-design/icons";
import customFetch from "utils/fetch";

const ArticleHeader = (props) => {
  const { data, match } = props;

  const { currentTags, setCurrentTags, submitLoading, submitHandler } = data;

  const [visible, setVisible] = useState(false);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const [availableTags, setAvailableTags] = useState([]);
  const [loading, setLoading] = useState(false);

  const getAllTags = async () => {
    setLoading(true);
    const res = await customFetch(`tags`, {
      method: "GET",
    });
    const { data } = await res.json();
    setLoading(false);
    setAvailableTags(data.map((tg) => tg.name));
  };

  useEffect(() => {
    getAllTags();
  }, []);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className="flex mtb-10 al-ctr">
      <div className="flex al-ctr j-end f11">
        <Tooltip title="Settings" className="mr-10">
          <Button
            shape="circle"
            size={isMobile ? "small" : "middle"}
            icon={<SettingOutlined />}
            onClick={showDrawer}
          />
        </Tooltip>
        {/* <Button className="mr-10" icon={<SaveOutlined />} size="large">
          Save draft
        </Button>
        <Button
          className="mr-10"
          type="primary"
          icon={<DeleteOutlined />}
          size="large"
          danger
          onClick={() => setShowDeleteModal(!showDeleteModal)}
        >
          Delete draft
        </Button> */}
        <Button
          className="mr-10"
          type="primary"
          icon={
            match.params.type === "edit" ? <SaveOutlined /> : <SendOutlined />
          }
          size={isMobile ? "small" : "large"}
          loading={submitLoading}
          onClick={submitHandler}
        >
          {match.params.type === "edit" ? "Save" : "Publish Now"}
        </Button>
      </div>
      <Modal
        title="Delete Article"
        visible={showDeleteModal}
        onOk={() => console.log("deleted")}
        onCancel={() => setShowDeleteModal(!showDeleteModal)}
        okButtonProps={{ danger: true }}
        cancelButtonProps={{ danger: true }}
        okText="Delete"
      >
        <Typography.Text>
          Are you sure you want to delete this piece?
        </Typography.Text>
      </Modal>
      <Drawer
        title="Article Settings"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={isMobile ? 250 : 450}
      >
        <Space direction="vertical">
          <div>
            <Typography.Text strong>Select tags (Upto 5)</Typography.Text>
            <div style={{ margin: "10px 0" }}>
              <Select
                mode="multiple"
                style={{ width: isMobile ? "200px" : "400px" }}
                placeholder="Select tags"
                value={currentTags}
                onChange={setCurrentTags}
                disabled={loading}
              >
                {currentTags.length < 5 &&
                  availableTags.map((tag) => (
                    <Select.Option key={tag}>{tag}</Select.Option>
                  ))}
              </Select>
            </div>
          </div>
          {/* <div className="mtb-10">
            <Checkbox
              checked={currentCommentsDisabled}
              onChange={() =>
                setCurrentCommentsDisabled(!currentCommentsDisabled)
              }
            >
              Disable Comments
            </Checkbox>
          </div> */}
        </Space>
      </Drawer>
    </div>
  );
};

export default withRouter(ArticleHeader);
