import { Layout } from "antd";
import { TopBar, SideBar } from "./index";

const { Content } = Layout;

const ApplicationLayout = ({ children }) => {
  return (
    <Layout className="layout">
      <TopBar />
      <Layout className="app-body">
        <SideBar />
        <Content>{children}</Content>
      </Layout>
    </Layout>
  );
};

export default ApplicationLayout;
