import { Layout } from "antd";
import { TopBar } from "./index";

const { Content } = Layout;

const CommonLayout = ({ children }) => {
  return (
    <Layout className="layout">
      <TopBar />
      <Layout className="app-body">
        <Content>{children}</Content>
      </Layout>
    </Layout>
  );
};

export default CommonLayout;
