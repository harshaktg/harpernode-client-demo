import styled from "styled-components";
import { HarperNode } from "assets/svg";

const LoaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  @keyframes spin-loader {
    0% {
      transform: rotate(0deg) scale(1);
    }
    65% {
      transform: rotate(0deg) scale(0.7);
    }
    100% {
      transform: rotate(359deg) scale(1);
    }
  }
  .logo-spin-loader {
    animation: spin-loader 1000ms linear infinite;
  }
`;

const Loader = (props) => {
  return (
    <LoaderWrapper>
      <HarperNode width="300" height="300" className="logo-spin-loader" />
    </LoaderWrapper>
  );
};

export default Loader;
