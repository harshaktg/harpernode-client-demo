import styled from "styled-components";
import { Link } from "react-router-dom";
import { UnAuthorized } from "assets/svg";

const UnAuthorizedWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 70px);
  .illus-desc {
    font-size: 24px;
  }
`;

const UnAuthorizedPage = (props) => {
  return (
    <UnAuthorizedWrapper>
      <UnAuthorized width="400" height="400" />
      <div className="illus-desc">
        Please <Link to="/register">Sign In</Link> to View
      </div>
    </UnAuthorizedWrapper>
  );
};

export default UnAuthorizedPage;
