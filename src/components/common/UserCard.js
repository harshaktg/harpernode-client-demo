import styled from "styled-components";
import { withRouter } from "react-router-dom";
import { Card, Typography, Avatar } from "antd";
import dayjs from "dayjs";

import Animation from "components/common/Animation";
import { UserOutlined } from "@ant-design/icons";

const UserCardWarpper = styled.div`
  height: 155px;
  .mt-15 {
    margin-top: 15px;
  }
  .post-name {
    cursor: pointer;
    font-size: 18px;
  }
  .post-footer {
    font-weight: bold;
  }
`;

const UserCard = (props) => {
  const { data, history } = props;

  return (
    <Card>
      <Animation>
        <UserCardWarpper>
          <div className="flex">
            <div className="mr-20">
              <Avatar icon={<UserOutlined />} src={data?.picture} size={60} />
            </div>
            <div className="f11">
              <Typography.Paragraph
                strong
                className="post-name"
                onClick={() => history.push(`/profile/view/${data.user_id}`)}
              >
                {data?.name}
              </Typography.Paragraph>
              <Typography.Paragraph type="secondary">
                {`Member since ${dayjs(data.__createdtime__).format(
                  "MMM DD YYYY"
                )}`}
              </Typography.Paragraph>
            </div>
          </div>
          <div className="mtb-10">
            <Typography.Paragraph italic ellipsis>
              {data.tagLine || "No tagline"}
            </Typography.Paragraph>
          </div>
          <div>
            <Typography.Paragraph type="secondary" ellipsis>
              {data.location || "Unknown location"}
            </Typography.Paragraph>
          </div>
        </UserCardWarpper>
      </Animation>
    </Card>
  );
};

export default withRouter(UserCard);
