import styled from "styled-components";
import { PageNotFound } from "assets/svg";
// import UnsplashReact, {
//   InsertIntoApplicationUploader,
//   withDefaultProps,
// } from "unsplash-react";

const NotFoundWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 70px);
`;

const NotFound = (props) => {
  return (
    <NotFoundWrapper>
      {/* <div style={{ height: "350px", width: "460px" }}>
        <UnsplashReact
          columns={3}
          accessKey={process.env.REACT_APP_UNSPLASH_ACCESS_KEY}
          Uploader={InsertIntoApplicationUploader}
          onFinishedUploading={(res) => console.log(res)}
        />
      </div> */}
      <PageNotFound width="70%" height="50%" />
    </NotFoundWrapper>
  );
};

export default NotFound;
