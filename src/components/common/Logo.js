import styled from "styled-components";
import { withRouter } from "react-router-dom";
import { HarperNode } from "assets/svg";

const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  .app-logo {
    margin: 0 10px;
    display: flex;
    align-items: center;
    img {
      height: 40px;
    }
  }
  .app-title {
    font-size: 24px;
    font-weight: 500;
  }
`;

const Logo = (props) => {
  const { history } = props;
  return (
    <LogoWrapper onClick={() => history.push("/")}>
      <div className="app-logo">
        {/* <img src={"/img/HarperNode.png"} alt={`HarperNode logo`} /> */}
        <HarperNode width={40} height={40} />
      </div>
      <div className="app-title">HarperNode</div>
    </LogoWrapper>
  );
};

export default withRouter(Logo);
