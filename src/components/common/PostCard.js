import { useState, useContext } from "react";
import styled from "styled-components";
import { withRouter, Link } from "react-router-dom";
import customFetch from "utils/fetch";
import { Card, Typography, Avatar, Tag, Button, Modal, message } from "antd";
import dayjs from "dayjs";
import Markdown from "markdown-to-jsx";
import Animation from "components/common/Animation";
import { UserOutlined } from "@ant-design/icons";
import AppContext from "contexts/AppContext";

const PostCardWarpper = styled.div`
  .mt-15 {
    margin-top: 15px;
  }
  .post-name {
    cursor: pointer;
    font-size: 16px;
  }
  .post-footer {
    font-weight: bold;
  }
`;

const PostCard = (props) => {
  const { data, history, triggerReload } = props;
  const context = useContext(AppContext);

  const [deleteLoading, setDeleteLoading] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const handleDeletePost = async () => {
    setDeleteLoading(true);
    const res = await customFetch(`posts/${data.post_id}`, {
      method: "DELETE",
    });
    const { data: newData, error } = await res.json();
    setShowDeleteModal(false);
    setDeleteLoading(false);
    if (newData) {
      triggerReload();
      message.success(newData);
    } else {
      message.error(error);
    }
  };

  return (
    <Card>
      <Animation>
        <PostCardWarpper>
          <div className="flex fwrap">
            <div className="mr-20">
              <Avatar
                icon={<UserOutlined />}
                src={data.created_user?.picture}
                size={60}
              />
            </div>
            <div className="f11">
              <Typography.Paragraph
                strong
                className="post-name"
                onClick={() =>
                  history.push(`/profile/view/${data.created_user?.user_id}`)
                }
              >
                {data.created_user?.name}
              </Typography.Paragraph>
              <Typography.Paragraph type="secondary">
                {dayjs(data.__createdtime__).format("MMM DD YYYY, hh:mm A")}
              </Typography.Paragraph>
            </div>
            {context.user &&
              context.user?.user_id === data.created_user?.user_id && (
                <div className="flex fwrap">
                  <Button
                    type="primary"
                    size="small"
                    className="mr-10 mb-10"
                    onClick={() =>
                      history.push({
                        pathname: `/article/edit/${data.post_id}`,
                        state: { data },
                      })
                    }
                  >
                    Edit
                  </Button>
                  <Button
                    type="primary"
                    size="small"
                    danger
                    className="mb-10"
                    onClick={() => setShowDeleteModal(!showDeleteModal)}
                  >
                    Delete
                  </Button>
                </div>
              )}
          </div>
          <div>
            <div className="mtb-10">
              <Typography.Title level={2} ellipsis>
                <Link to={`/article/view/${data.post_id}`}>{data.title}</Link>
              </Typography.Title>
            </div>
            <div>
              <Typography.Paragraph
                ellipsis={{
                  rows: 2,
                  expandable: false,
                }}
              >
                <Markdown>{data.content}</Markdown>
              </Typography.Paragraph>
            </div>
          </div>
          <div className="mb-10">
            {data.tags?.map((tag) => (
              <Tag>{tag}</Tag>
            ))}
          </div>
        </PostCardWarpper>
      </Animation>
      <Modal
        title="Delete Article"
        visible={showDeleteModal}
        onOk={handleDeletePost}
        onCancel={() => setShowDeleteModal(!showDeleteModal)}
        okButtonProps={{ danger: true }}
        cancelButtonProps={{ danger: true }}
        okText="Delete"
        confirmLoading={deleteLoading}
      >
        <Typography.Text>
          Are you sure you want to delete this post?
        </Typography.Text>
      </Modal>
    </Card>
  );
};

export default withRouter(PostCard);
