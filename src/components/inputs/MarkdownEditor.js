import React, { Component } from "react";
import * as MarkdownIt from "markdown-it";
import MdEditor, { Plugins } from "react-markdown-editor-lite";
import styled from "styled-components";
import "react-markdown-editor-lite/lib/index.css";

const OuterWrapper = styled.div`
  &.hide {
    display: none;
  }
  .editor-container {
    .section {
      &.visible {
        :nth-child(1) {
          border-right: 1px solid #e0e0e0;
        }
      }
    }
  }
`;

MdEditor.unuse(Plugins.FullScreen);
// MdEditor.unuse(Plugins.ModeToggle);
MdEditor.unuse(Plugins.FontUnderline);
MdEditor.unuse(Plugins.Clear);
MdEditor.use(Plugins.TabInsert, {
  tabMapValue: 1,
});

class MarkdownEditor extends Component {
  constructor(props) {
    super(props);
    this.mdEditor = React.createRef();
    this.mdParser = new MarkdownIt({
      html: true,
      linkify: true,
      typographer: true,
      highlight(str, lang) {},
    });

    // this.state = {
    //   value: this.props.value || "",
    // };
  }

  handleEditorChange = (it, event) => {
    // this.setState({
    //   value: it.text,
    // });
    this.props.updateHandler(it.text);
  };

  handleImageUpload = (file) => {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.onload = (data) => {
        resolve(data.target.result);
      };
      reader.readAsDataURL(file);
    });
  };

  handleGetMdValue = () => {
    if (this.mdEditor.current) {
      alert(this.mdEditor.current.getMdValue());
    }
  };

  handleGetHtmlValue = () => {
    if (this.mdEditor.current) {
      alert(this.mdEditor.current.getHtmlValue());
    }
  };

  // handleSetValue = () => {
  //   const text = window.prompt("Content");
  //   this.setState({
  //     value: text,
  //   });
  // };

  renderHTML = (text) => {
    return this.mdParser.render(text);
  };

  render() {
    const { isHidden, placeholder, value } = this.props;
    return (
      <OuterWrapper className={isHidden ? "hide" : ""}>
        <div className="editor-wrap">
          <MdEditor
            ref={this.mdEditor}
            value={value}
            style={{ height: "calc(100vh - 347px)", width: "100%" }}
            renderHTML={this.renderHTML}
            placeholder={placeholder}
            readOnly={isHidden}
            config={{
              shortcuts: true,
              view: {
                menu: true,
                md: true,
                html: true,
              },
              table: {
                maxRow: 5,
                maxCol: 6,
              },
              syncScrollMode: ["leftFollowRight", "rightFollowLeft"],
              imageAccept: ".jpg,.png",
            }}
            onChange={this.handleEditorChange}
            onImageUpload={this.handleImageUpload}
          />
        </div>
      </OuterWrapper>
    );
  }
}

export default MarkdownEditor;
