import { Card, Skeleton } from "antd";

const ProfileSkeleton = (props) => {
  const { buttons } = props;
  const total = Array.from({ length: buttons || 1 }, () => 0);
  return (
    <div>
      {buttons > 0 && (
        <div className="flex mtb-10 al-ctr">
          <div className="flex al-ctr j-end f11">
            {total.map((btn) => (
              <Skeleton.Button active className="mr-10" size="large" />
            ))}
          </div>
        </div>
      )}
      <Card className="profile-card mtb-10">
        <div className="flex">
          <Skeleton.Avatar size={150} active className="mr-10" />
          <Skeleton active />
        </div>
      </Card>
      <Card className="user-content">
        <Skeleton active />
        <Skeleton active />
        <Skeleton active />
      </Card>
    </div>
  );
};

export default ProfileSkeleton;
