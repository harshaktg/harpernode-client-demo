import { Card, Skeleton } from "antd";

const PostCardSkeleton = (props) => {
  return (
    <Card>
      <div className="flex">
        <Skeleton.Avatar size={60} active className="mt-15 mr-20" />
        <Skeleton active paragraph={{ rows: 1 }} />
      </div>
      <div>
        <Skeleton active />
      </div>
    </Card>
  );
};

export default PostCardSkeleton;
