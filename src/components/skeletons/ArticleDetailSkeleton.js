import { Card, Skeleton } from "antd";

const ArticleDetailSkeleton = (props) => {
  return (
    <Card>
      <Skeleton active paragraph={{ rows: 8 }} />
    </Card>
  );
};

export default ArticleDetailSkeleton;
