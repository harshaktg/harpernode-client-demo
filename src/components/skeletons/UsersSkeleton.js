import { Card, Skeleton } from "antd";

const gridStyle = {
  width: "calc(25% - 20px)",
  margin: "10px",
  textAlign: "center",
};

const UsersSkeleton = (props) => {
  const { size } = props;
  const total = Array.from({ length: size || 8 }, () => 0);
  return (
    <>
      {total.map((card) => (
        <Card.Grid style={gridStyle}>
          <div>
            <Skeleton.Avatar size={80} active className="mr-10" />
            <Skeleton active paragraph={{ rows: 2 }} />
          </div>
        </Card.Grid>
      ))}
    </>
  );
};

export default UsersSkeleton;
