import { Row, Col, Skeleton } from "antd";
import ArticleDetailSkeleton from "./ArticleDetailSkeleton";

const ViewArticleSkeleton = (props) => {
  return (
    <Row gutter="16">
      <Col className="gutter-row" span={16}>
        <Skeleton active />
      </Col>
      <Col className="gutter-row" span={8}>
        <ArticleDetailSkeleton />
      </Col>
    </Row>
  );
};

export default ViewArticleSkeleton;
