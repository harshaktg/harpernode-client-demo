import AppContext from "contexts/AppContext";
import UnAuthorizedPage from "components/common/UnAuthorized";
import { useContext } from "react";

const WithAuthorized = (props) => {
  const context = useContext(AppContext);

  if (!context.user) {
    return <UnAuthorizedPage />;
  }

  return <>{props.children}</>;
};

export default WithAuthorized;
